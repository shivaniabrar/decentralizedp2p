package serverList

import (
	"github.com/olebedev/config"
//	"fmt"
	"strconv"
	"net/rpc"
	"time"
	"math/rand"
)

type Server struct{
	id string
	ip string
	port string
	dhtport string
	conn *rpc.Client
	dhtconn *rpc.Client
}

type ServerList struct {
	list map[string]*Server
	totServers int
	Replicationfactor int
	FileReplicationfactor int
}

func (s *ServerList) InitServerList(config_file string) (err error) {
	
	s.totServers = 0
	s.list = make(map[string]*Server)
	
	cfg,err := config.ParseYamlFile(config_file)
	if err!= nil {
		return
	}

	// Init server table 
	for {
		id, err := cfg.String("servers."+strconv.Itoa(s.totServers)+".id")
		if err != nil {
			break
		}
		ip, err := cfg.String("servers."+strconv.Itoa(s.totServers)+".ip")
		if err != nil {
			break
		}
		port, err := cfg.String("servers."+strconv.Itoa(s.totServers)+".port")
		if err != nil {
			break
		}
		dhtport, err := cfg.String("servers."+strconv.Itoa(s.totServers)+".dhtport")
		if err != nil {
			break
		}				
		s.list[strconv.Itoa(s.totServers)]= &Server{id,ip,port,dhtport,nil,nil}
		s.totServers += 1
	}

	replicationfactor, err := cfg.Int("replication.0.dhtreplicas")
	filereplicationfactor, err := cfg.Int("replication.0.filereplicas")
	if err == nil {
		s.Replicationfactor = replicationfactor
		s.FileReplicationfactor = filereplicationfactor	
	}
	return
}

func (s *ServerList) CheckId(id string) (bool){
	server := s.list[id]
	if server == nil{
		return false
	}
	return true
}

func (s *ServerList) GetId(index string) (id string){
	id = s.list[index].id
	return
}

func (s *ServerList) GetIP(id string) (ip string){
	ip = s.list[id].ip
	return
}

func (s *ServerList) GetPort(id string) (port string){
	server := s.list[id]
	if server == nil {
		port = ""
		return
	}
	port = server.dhtport		
	return
}

func (s *ServerList) GetServerPort(id string) (port string){
	server := s.list[id]
	if server == nil {
		port = ""
		return
	}
	port = server.port		
	return
}


func (s *ServerList) GetConnection(id string) (conn *rpc.Client,err error){
	conn = s.list[id].conn
	if conn == nil{
		conn,err = s.MakeConnection(id)
	}
	s.list[id].conn = conn
	return
}

func (s *ServerList) GetDhtConnection(id string) (dhtconn *rpc.Client,err error){
	dhtconn = s.list[id].dhtconn
	if dhtconn == nil{
		dhtconn,err = s.MakeConnection(id)
	}
	s.list[id].dhtconn = dhtconn
	return
}

func (s *ServerList) MakeDhtConnection(id string) (conn *rpc.Client,err error){
	conn,err = rpc.Dial("tcp", s.list[id].ip + ": " + s.list[id].dhtport)
	if err!= nil {
		conn = nil
	}
	return
}


func (s *ServerList) MakeConnection(id string) (conn *rpc.Client,err error){
	conn,err = rpc.Dial("tcp", s.list[id].ip + ": " + s.list[id].port)
	if err!= nil {
		conn = nil
	}
	return
}

func (s *ServerList) GetTotalNumberofServers() (totServers int){
	totServers = s.totServers
	return
}

func (s *ServerList) GetRandomId() (id string){
	rand.Seed(int64(time.Now().Nanosecond()))
	id = strconv.Itoa(rand.Intn(s.totServers))
	return
}

func (s *ServerList) GetNextId(id string) (string){
	new_id,_ := strconv.Atoi(id)
	new_id = (new_id+1) % s.totServers
	return strconv.Itoa(new_id)
}