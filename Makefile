CC = go
CFLAGS = install
CLIENT  = client
SERVER  = server
PERF = performance

GOPATH := ${PWD}
export GOPATH

all: build

build:  clean
	$(CC) $(CFLAGS) $(CLIENT)
	$(CC) $(CFLAGS) $(SERVER)
	$(CC) $(CFLAGS) $(PERF)

clean:
	rm -rf ./pkg/
	rm -rf ./bin/

