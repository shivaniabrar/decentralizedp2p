package main

import (
	"fmt"
	"flag"
	"peer"
	"serverList"
	"strconv"
)

var menu = []string{"Replicate", "Obtain"}

func main() {

	// Get user parameters
	config,dir := getUserParamaters()
	
	// Create Server list object to parse config file
	servers := new(serverList.ServerList)
	peer.RegisterTypes()
	Peer := new(peer.Peer)
	
	//Init server list object
	err := servers.InitServerList(config)
	if err!=nil {
		fmt.Println("Yaml: Configuration Error")
		return
	}
	//Init peer object and start servers
	err = Peer.InitPeer(servers,dir)
	if err!=nil {
		fmt.Println(err)
		return
	}

	//Register peer
	status := Peer.Register()
	if !status {
		fmt.Println("Registration Failed")
		return
	}

	fmt.Println("Registration Successful")
	fmt.Printf("Peer Info: %s:%s\n",Peer.ID.IP,Peer.ID.Port)

	for {
		runUI(Peer)
	}

}


func getUserParamaters() (string,string) {
	config := flag.String("config", "./servers.yml", "Config file: Path of file storing server list")
	dir := flag.String("dir", ".", "Client's directory")
	flag.Parse()
	return *config,*dir
}

func runUI(peer *peer.Peer) {
	var choice int
	var filename string
	var str string
	fmt.Print("\nEnter filename with extension: ")
	fmt.Scanf("%s",&filename)


	fmt.Println("\nWhat you would like to do?: \n")
	for index, operation := range menu {
		/* Since index starts from 0 */
		fmt.Printf("%d. %s\n", index+1, operation)
	}
	
	fmt.Print("Enter your choice: ")
	fmt.Scanf("%s", &str)
	choice,err:=strconv.Atoi(str)
	
	switch {
	case (err!=nil) || (choice > len(menu)) || (choice <= 0):
		fmt.Println("Invalid Input")

	case choice == 1:
		status := peer.Replicate(filename)
		if status {
			fmt.Println("File replicated successfully")
		}else {
			fmt.Println("File replication failed")
		}

	case choice == 2:
		peers := peer.Search(filename)

		if peers == nil {
			fmt.Println("No peers is having file")
			break
		}

		fmt.Println("Peers that have file '",filename,"': \n")
		for i := 0; i < len(peers); i++ {
			fmt.Printf("%d. %s:%s\n",i+1,peers[i].IP,peers[i].Port)
		}

		fmt.Print("\nFrom which peer you want to download file? ")
		fmt.Scanf("%s",&str)
		id,err := strconv.Atoi(str)
		if (id>len(peers)) || (id <= 0) || (err!=nil){
			fmt.Println("Error: Invalid peer")
			break
		}

		fmt.Println("Downloading file...")
		status,speed := peer.Obtain(peer.Path, filename,peers,id-1)
		if status {
			fmt.Printf("Download Successfully @ %.4f bytes/second\n",speed)
		}else {
			fmt.Println("Download Failed")
		}			
	}
}