#!/bin/bash

#echo "operation servers peers latency ops" > ./result1.txt
#echo "operation servers peers latency ops" > ./result2.txt
echo "size Bps" > ./result3.txt
echo "size Bps" > ./result4.txt
echo "Time for runs" > ./time3.txt
echo "Time for runs" > ./time4.txt
export GOMAXPROCS=1

: <<'END'
for i in 1 2 4 8
do
    ./run_test.sh -s 1 -p $i -o ./result1.txt
    sleep 15
done

for i in 1 2 4 8
do
    ./run_test.sh -s $i -p $i -o ./result2.txt
    sleep 15
done
END

for i in 1K 10K 100K 1M 10M 100M 1G
do
    START=$(gdate +%s.%N)
    ./run_test.sh -s 1 -p 8 -o ./result3.txt -l $i
    END=$(gdate +%s.%N)
    DIFF=$(echo "$END - $START" | bc)
    echo "Time for " $i " = " $DIFF >> ./time3.txt
    sleep 15
done

for i in 1K 10K 100K 1M 10M 100M 1G
do
    START=$(gdate +%s.%N)
    ./run_test.sh -s 8 -p 8 -o ./result4.txt -l $i
    END=$(gdate +%s.%N)
    DIFF=$(echo "$END - $START" | bc)
    echo "Time for " $i " = " $DIFF >> ./time4.txt
    sleep 15
done
