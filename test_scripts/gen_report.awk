#!/usr/bin/awk -f

BEGIN {
	r_count = 0;
	s_count = 0;
	o_count = 0;
	r_lat = 0.0;
	s_lat = 0.0;
	o_lat = 0.0;
	r_ops = 0.0;
	s_ops = 0.0;
	o_ops = 0.0;

}

/register:/ {
	r_lat += $2;
	r_ops += (1.0 / $2);
	++r_count;
}

/search:/ {
	s_lat += $2;
	s_ops += (1.0 / $2);
	++s_count;
}

/obtain:/ {
	o_lat += $2;
	o_ops += (1.0 / $2);
	++o_count;
}

/K:|M:|G:/ {
	if ($1 in Bps) {
		Bps[$1] += $2;
	} else {
		Bps[$1] = $2;
	}
}

END {
	if (r_count != 0) {
		print "register", servers, clients, (r_lat / r_count)*1000000, r_ops; 
	}

	if (s_count != 0) {
		print "search", servers, clients, (s_lat / s_count)*1000000, s_ops;
	}

	if (o_count != 0) {
		print "obtain", servers, clients, (o_lat / o_count)*1000000, o_ops;
	}

	for (size in Bps) {
		printf("%s %f\n", size, Bps[size])
	}
}
