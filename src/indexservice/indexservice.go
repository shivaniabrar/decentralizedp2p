package indexservice

import (
//	"fmt"
	"serverList"
	"distributedhash"
	"hashservice"
//	"time"
	//"encoding/gob"
//	"container/list"
	"encoding/json"
	"net/rpc"
//	"strings"
//	"utility"
//	"utility/service"
)


type Id struct {
	IP string `json:"ip"`
	Port string `json:"port"`
	RPC_PORT string `json:"rpc_port"`
}


type Peer struct{
	ID Id
	Filelist []string
	Filename string
	Serverlist *serverList.ServerList
	Path string
}


type Index struct
{
	dht *distributedhash.Dht
	serverlist *serverList.ServerList
}

func InitIndexService(id string, servers *serverList.ServerList,hashService *hashservice.Service) (index *Index) {
	index = new(Index)
	index.dht = new(distributedhash.Dht)
	index.serverlist = servers	
	index.dht.InitDht(servers,id,hashService)
	return
}

/*
	Registers Client
	Index: Associates filenames with clients
*/
func (index *Index) Register(peer Peer, ack *bool) error {
	fileprefix := "file_"
	replicationfactor := index.dht.GetReplicationfactor()
	
	index.dht.SetReplicationfactor(index.serverlist.GetTotalNumberofServers())
	index.dht.PutArray("Peers:",peer.ID)
	index.dht.SetReplicationfactor(replicationfactor)
	
	for i := 0; i < len(peer.Filelist); i++ {
		index.dht.PutArray(fileprefix+peer.Filelist[i],peer.ID)
	}
	//fmt.Println("Peer ", peer.ID, " num files ", len(peer.Filelist))
	//peers,_ := index.dht.Get(fileprefix+"test1")
	//fmt.Println(" test1 value = ", peers)
	*ack = true
//	fmt.Printf("\n Peer<%s,%s> Registered\n", peer.ID.IP, peer.ID.Port)
	return nil
}

/*
	Search files in Index
*/
func (index *Index) Searchfile(filename string, bytes *[]byte) error {
	fileprefix := "file_"
	peers,_ := index.dht.Get(fileprefix+filename)
	*bytes, _ = json.Marshal(peers)
	return nil
}

/*
	Updates Index
	If file download or replicated index must be updated
*/
func (index *Index) Updatefilelist(peer Peer, ack *bool) error {
	fileprefix := "file_"
	index.dht.PutArray(fileprefix+peer.Filename,peer.ID)
	return nil
}

/*
	Informs Other Peers to Replicate file
*/
func (index *Index) Replicate(peer Peer, ack *bool) error {
	var reply bool
	*ack = true

	/*
		Get Number of Replicas
	*/
	replicas := index.serverlist.FileReplicationfactor
	
	if replicas<=0 {
		*ack = false
		return nil	
	}

	fileprefix := "file_"
	filename := peer.Filename

	replicationfactor := index.dht.GetReplicationfactor()
	index.dht.SetReplicationfactor(index.serverlist.GetTotalNumberofServers())
	totPeers,_ := index.dht.Get("Peers:")
	index.dht.SetReplicationfactor(replicationfactor)
	
	
	peers,_:= index.dht.Get(fileprefix+filename)

	if peers == nil {
		*ack = false
		return nil
	}

	authenticate := true
	for _, clientIds := range (peers.([]interface{})) {
		if ((clientIds.(Id).IP == peer.ID.IP) && (clientIds.(Id).Port == peer.ID.Port))  == true {
				authenticate = false
				break
		}
	}

	if authenticate {
		*ack = false
		return nil
	}

	/*
		Get total number of clients currently having the file
	*/
	no_of_clients := len(peers.([]interface{}))
	/*
		Get total number of clients registered
	*/

	total_clients := len(totPeers.([]interface{}))

	if no_of_clients >= replicas {
		return nil
	}

	if total_clients < replicas {
		*ack=false
		return nil
	}

	required_clients := replicas - no_of_clients

	for _, clientIds := range peers.([]interface{}) {
		for e := 0; e < len(totPeers.([]interface{})); e += 1 {
			if required_clients <= 0 {
					break
				}
			if ((clientIds.(Id).IP == (totPeers.([]interface{})[e].(Id)).IP) && (clientIds.(Id).Port == (totPeers.([]interface{})[e].(Id)).Port)) == false {
				c, err := rpc.Dial("tcp", (totPeers.([]interface{})[e].(Id)).IP+": "+(totPeers.([]interface{})[e].(Id)).RPC_PORT)
				if err != nil {
					*ack = false
					return nil
				}
				err = c.Call("Listener.Replicate", peer, &reply)
				if err != nil {
					*ack = false
					return nil
				}
				required_clients -= 1	
			}
		}
	}

	return nil
}