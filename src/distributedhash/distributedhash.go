package distributedhash

import 
(
	"serverList"
	"encoding/gob"
    "bytes"
    "utility"
    "strconv"
    "net/rpc"
    "fmt"
    "crypto/sha1"
    "encoding/binary"
    "hashservice"
)

type Dht struct{
	servers *serverList.ServerList
	id string
	replicationfactor int
	hashService *hashservice.Service
}

func (dht *Dht) InitDhtWithServers(path string) (err error){
	servers := new(serverList.ServerList)
	err = servers.InitServerList(path)
	if err!=nil {
		return
	}
	dht.InitDht(servers,"",nil)
	return
}

func (dht *Dht) InitDht(servers *serverList.ServerList,id string,hashService *hashservice.Service){
	dht.servers = servers
	dht.id = id
	if servers.Replicationfactor<=0 {
		servers.Replicationfactor = 1
	}
	dht.replicationfactor = servers.Replicationfactor
	dht.hashService = hashService
	gob.Register([]interface{}{})
}

func (dht *Dht) SetReplicationfactor(replicationfactor int) {
	dht.replicationfactor = replicationfactor
}

func (dht *Dht) GetReplicationfactor() int {
	return dht.replicationfactor
}

func (dht *Dht) Get(key interface{}) (value interface{},status bool){
	var err error
	var conn *rpc.Client

	kv := utility.KV{key,nil}
	index := hash(kv.GetKey(),dht.servers.GetTotalNumberofServers())
	status = false

	if dht.id == dht.servers.GetId(strconv.Itoa(index)) {
		status=true
		dht.hashService.Get(kv,&kv)
		value = kv.GetValue()
		return
	}

	for i := 0; i < dht.replicationfactor ; i++ {
		// Gets connection
		conn,err = dht.getConnection(index)
		if err == nil {
			err = conn.Call("Service.Get",kv, &kv)
			if err==nil {
				status = true
				//fmt.Println("Get from server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),">")
				break
			}
		}
		//if server is down get next server for replica
		//fmt.Println("Get from server < (id ip port): ",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> fail")
		index = GetNextIndex(index,dht.servers.GetTotalNumberofServers())
	}

	value = kv.GetValue()
	return
}

func (dht *Dht) Put(key interface{}, value interface{}) (status bool){
	var reply bool
	
	status = false
	kv := utility.KV{key,value}
	index := hash(kv.GetKey(),dht.servers.GetTotalNumberofServers())

	for i := 0; i < dht.replicationfactor; i++ {
		if dht.id == dht.servers.GetId(strconv.Itoa(index)) {
			status=true
			dht.hashService.Put(kv,&reply)
			index = GetNextIndex(index,dht.servers.GetTotalNumberofServers())
			continue
		}

		conn,err := dht.getConnection(index)
		if err == nil {
			err = conn.Call("Service.Put",kv, &reply)
			if err == nil {
			//	fmt.Println("Put on server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> Sucessfull")
				status = true
			}
		}
		if err!=nil{
			//fmt.Println("Put on server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> Fail")
		}
		index = GetNextIndex(index,dht.servers.GetTotalNumberofServers())
	}
	return
}

func (dht *Dht) Delete(key interface{}) (value interface{}){
	kv := utility.KV{key,nil}
	index := hash(kv.GetKey(),dht.servers.GetTotalNumberofServers())

	for i := 0; i < dht.replicationfactor; i++ {
		conn,err := dht.getConnection(index)
		if err == nil {
			err = conn.Call("Service.Delete",kv, &kv)
			if (err == nil){
				//Server is up
				if (kv.GetValue()!=nil){
					value = kv.GetValue()
					fmt.Println("Delete on server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> Sucessfull")
				}else{
					fmt.Println("Delete on server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> Failed : Value doesn't exist")
				}
			}			
		}
		if err!=nil{
			fmt.Println("Delete on server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> Failed")
		}
		index = GetNextIndex(index,dht.servers.GetTotalNumberofServers())
	}
	return
}

func (dht *Dht) PutArray(key interface{}, value interface{}) (status bool){
	var reply bool
	
	status = false
	kv := utility.KV{key,value}
	index := hash(kv.GetKey(),dht.servers.GetTotalNumberofServers())
	for i := 0; i < dht.replicationfactor; i++ {
		if dht.id == dht.servers.GetId(strconv.Itoa(index)) {
			status=true
			dht.hashService.PutArray(kv,&reply)
			index = GetNextIndex(index,dht.servers.GetTotalNumberofServers())
			continue
		}
		conn,err := dht.getConnection(index)
		if err == nil {
			err = conn.Call("Service.PutArray",kv, &reply)
			if err == nil {
				//fmt.Println("Put on server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> Sucessfull")
				status = true
			}
		}
		if err!=nil{
			//fmt.Println("Put on server < (id ip port):",dht.servers.GetId(strconv.Itoa(index)),dht.servers.GetIP(strconv.Itoa(index)),dht.servers.GetPort(strconv.Itoa(index)),"> Fail")
		}
		index = GetNextIndex(index,dht.servers.GetTotalNumberofServers())
	}
	return
}



func (dht *Dht) getConnection(index int)(conn *rpc.Client,err error){
	conn,err = dht.servers.GetDhtConnection(strconv.Itoa(index))
	if conn == nil{
		conn,err = dht.servers.MakeDhtConnection(strconv.Itoa(index))
	}
	return
}

func hash(key interface{},size int) int{
	b,_ := GetBytes(key)
	hasher := sha1.New()
	hasher.Write(b)
	data := binary.LittleEndian.Uint64(hasher.Sum(nil))
	index := int(data % uint64(size))
	return index
}

func GetNextIndex(index int,size int) int{
	index = (index+1)%size
	return index
}

func GetBytes(key interface{}) ([]byte, error) {
    var buf bytes.Buffer
    enc := gob.NewEncoder(&buf)
    err := enc.Encode(key)
    if err != nil {
        return nil, err
    }
    return buf.Bytes(), nil
}