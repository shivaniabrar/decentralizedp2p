package hashservice

import (
	"utility"
	"sync"
	"encoding/gob"
)

type Service struct{
	Mutex sync.RWMutex
	htable map[interface{}]interface{}
}

func InitService(s *Service){
	s.htable  = make(map[interface{}]interface{})
	gob.Register([]interface{}{})	
}

func (s* Service) Get(kv utility.KV,kvUpdate *utility.KV) error{
	s.Mutex.RLock()
	kvUpdate.PutValue(s.htable[kv.GetKey()])
	s.Mutex.RUnlock()
	return nil
}

func (s* Service) Put(kv utility.KV,status* bool) error{
	s.Mutex.Lock()
	s.htable[kv.GetKey()] = kv.GetValue()
	s.Mutex.Unlock()
	return nil
}

func (s* Service) Delete(kv utility.KV,kvUpdate *utility.KV) error{
	key := kv.GetKey()
	s.Mutex.Lock()
	value := s.htable[key]
	delete(s.htable,key)
	s.Mutex.Unlock()
	kvUpdate.PutValue(value)
	return nil
}

func (s* Service) PutArray(kv utility.KV,status* bool) error{
	s.Mutex.Lock()
	if s.htable[kv.GetKey()] == nil {
		s.htable[kv.GetKey()] = []interface{}{}
	}
	defer func(){
		if recover()!=nil{
			s.Mutex.Unlock()
			s.Put(kv,status)
		}
	}()
	s.htable[kv.GetKey()] = append(s.htable[kv.GetKey()].([]interface{}),kv.GetValue())
	s.Mutex.Unlock()
	return nil
}