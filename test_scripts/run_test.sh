#!/bin/bash

# default values of params
SERVERS=1
PEERS=1
OP_FILE="./result.txt"
SIZE="NONE"

while [[ $# > 1 ]]
do
key="$1"
case $key in
    -s|--num-servers)
    SERVERS="$2"
    shift # past argument
    ;;
    -p|--num-peers)
    PEERS="$2"
    shift # past argument
    ;;
    -o|--output-file)
    OP_FILE="$2"
    shift # past argument
    ;;
    -l|--size)
    SIZE="$2"
    shift # past argument
    ;;
    *)
    echo "Unknown option: " $1
    exit
    ;;
esac
shift # past argument or value
done

if [ "$SIZE" != "NONE" ]
then
    rm -rf ../testDataDir
    mkdir -p ../testDataDir
    python files.py -s $SIZE -d "../testDataDir/" 
    sleep 5
fi

PIDS[0]=0
for i in `seq 1 $SERVERS`
do
    sid=$((i - 1))
    server -id $sid -path config_$SERVERS.yml &
    PIDS[i]=$!
done

if [ "$SIZE" = "NONE" ]
then
	./run_peers.exp $PEERS $SERVERS
else
	./run_peers_2.exp $PEERS $SERVERS $SIZE
fi

cat op_* | ./gen_report.awk -v servers=$SERVERS clients=$PEERS >> $OP_FILE

for i in `seq 1 $SERVERS`
do
    kill ${PIDS[i]}
done

rm op_*

if [ "$SIZE" != "NONE" ]
then
    rm -rf ../testDataDir
fi
