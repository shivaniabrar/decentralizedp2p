package peer

import (
	"net/http"
	"os"
	"io"
	"utility"
	"encoding/gob"
	"strings"
	"utility/service"
//	"fmt"
	"serverList"
	"encoding/json"
	"utility/rpcserver"
	"time"
)

type Id struct {
	IP string `json:"ip"`
	Port string `json:"port"`
	RPC_PORT string `json:"rpc_port"`
}

type Peer struct{
	ID Id
	Filelist []string
	Filename string
	Serverlist *serverList.ServerList
	Path string
}

type Listener struct {
	peer *Peer
}

func (l *Listener) Replicate(peer Peer, ack *bool) error {
	l.peer.download(l.peer.Path, peer.Filename, peer.ID)
	return nil
}


func RegisterTypes() {
	gob.Register(Id{})	
	gob.Register(Peer{})
	gob.Register([]interface{}{})
}

/*
	InitPeer
		Parse config file
		Start File Server
		Start Server to listen request for replication
*/

func (peer *Peer) InitPeer(serverList *serverList.ServerList, path string) (err error){
	
	server := new(rpcserver.RpcServer)
	
	filelistner := service.GetListner()
	replicationService := new(Listener)

	server.InitRpcServer("","0")
	peer.ID = Id{utility.GetIP(),utility.GetPort(filelistner),server.PORT}
	FileList,err := utility.GetFilelist(path)
	peer.Filelist = strings.Split(string(FileList)," ")

	peer.Serverlist = serverList

	peer.Path = path
	replicationService.peer  = peer
	go service.StartFileServer(filelistner, path)
	go server.StartRpcServer(replicationService)
	return
}

/*
	Register
		Register with server
*/

func (peer *Peer) Register() (status bool){
	status = false
	id := peer.Serverlist.GetRandomId()
	
	for i:=0; i < peer.Serverlist.GetTotalNumberofServers(); i++ {
		conn,err := peer.Serverlist.GetConnection(id)
		if err==nil {
			err = conn.Call("Index.Register",*peer, &status)
			if err==nil {
				break				
			}
		}
		id = peer.Serverlist.GetNextId(id)
	}
	return
}

/*
	Search
		Search on server and return id's of peer having requested file
*/

func (peer *Peer) Search(filename string) ([]Id){
	
	var reply []byte
	var peerIds []Id

	id := peer.Serverlist.GetRandomId()

	for i:=0; i < peer.Serverlist.GetTotalNumberofServers(); i++ {
		conn,err := peer.Serverlist.GetConnection(id)
		if err == nil{
			err = conn.Call("Index.Searchfile", filename, &reply)
			if err == nil{
				break
			}
		}
		id = peer.Serverlist.GetNextId(id)
	}
	
	json.Unmarshal(reply, &peerIds)
	return peerIds
}

/*
	Obtain
		Send download request to another peer
*/

func (peer *Peer) Obtain(path string, filename string,peers []Id,id int) (status bool,speed float64){
	 remotePeer := peers[id]
	 status,speed = peer.download(path, filename,remotePeer)
	 return
}

/*
	download 
		Connect remote peer 
		Download file
		Update metadata on index server
*/
func (peer *Peer) download(path string, filename string,remotePeer Id) (status bool,speed float64){
	 var reply bool
	 status = false
	 peer.Filename = filename
	 id := peer.Serverlist.GetRandomId()
	 out, err := os.Create(path+"/"+filename)
	 if err != nil {
		return
	 }
	 defer out.Close()
	 now := time.Now()
	 response, err := http.Get("http://" + remotePeer.IP + ":" + remotePeer.Port + "/" + filename)	
	 if err != nil {
		return
	 }
	defer response.Body.Close()
	bytes,_ :=io.Copy(out, response.Body)
	duration := time.Since(now).Seconds()
	speed = float64(bytes)/duration
	for i:=0; i < peer.Serverlist.GetTotalNumberofServers(); i++ {
		conn,err := peer.Serverlist.GetConnection(id)
		if err==nil {
			err = conn.Call("Index.Updatefilelist", peer, &reply)
			if err==nil {
				break
			}					
		}
		id = peer.Serverlist.GetNextId(id)
	}
	
	if err==nil {	
		status = true
	}
	return
}

/*
	Replicate
		Send request to index server to request other peers to pull the file
*/
func (peer *Peer) Replicate(filename string) (status bool){
	status = false
	peer.Filename = filename
	id := peer.Serverlist.GetRandomId()
	for i:=0; i < peer.Serverlist.GetTotalNumberofServers(); i++ {
		conn,err := peer.Serverlist.GetConnection(id)
		if err ==nil {
			err = conn.Call("Index.Replicate",*peer, &status)
			if err==nil {
				return
			}
		}
		id = peer.Serverlist.GetNextId(id)
	}	
	return
}