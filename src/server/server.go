package main

import (
	"fmt"
	"hashservice"
	"utility/rpcserver"
	"serverList"
	"peer"
	"flag"
	"encoding/gob"
	"indexservice"
)


func main() {


	var id string
	var path string

	id,path = getUserParamaters()	
	
	GOB_Register()
	servers,err,status := getServers(id,path)
	
	if err!=nil {
		fmt.Println("Yaml: Configuration Error")
		return
	}

	if status == false {
		fmt.Println("Invalid id:", id)
		return
	}

	//Starts hash service which exposes an api to get, put and delete key/values
	hashService := StartDht(id,servers)
	//Starts index server	
	StartServer(id,servers,hashService)

}

func getUserParamaters() (string,string) {
	id := flag.String("id", "0", "id of server")
	path := flag.String("path", "./servers.yml", "Path of file storing server list")
	flag.Parse()
	return *id,*path
}

func GOB_Register() {
	peer.RegisterTypes()
	gob.Register(indexservice.Id{})
	gob.Register(indexservice.Peer{})
	gob.Register([]interface{}{})
}

/*
	Gets server list
*/
func getServers(id string,path string)(servers *serverList.ServerList, err error, status bool) {
	servers = new(serverList.ServerList)
	err = servers.InitServerList(path)
	status = servers.CheckId(id)
	return
}

/*
	Starts dht server 
*/
func StartDht(id string, servers *serverList.ServerList) (hashService *hashservice.Service){
	port := servers.GetPort(id)
	server := new(rpcserver.RpcServer)
	server.InitRpcServer("",port)	
	hashService = new(hashservice.Service)
	hashservice.InitService(hashService)
	go server.StartRpcServer(hashService) //Start Server
	return
}

/*
	Starts index server
	Print IP and Port of Server (only to verify; since ip and port are mentioned in config file)
*/
func StartServer(id string, servers *serverList.ServerList,hashService *hashservice.Service) {
	server := new(rpcserver.RpcServer)
	server.InitRpcServer("",servers.GetServerPort(id))
	indexService := indexservice.InitIndexService(id, servers,hashService)
	fmt.Println("Server Information")
	fmt.Printf("IP: %s Port:%s\n\n", server.IP, server.PORT)
	server.StartRpcServer(indexService)
}