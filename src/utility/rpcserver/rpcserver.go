package rpcserver

import (
	"net"
	"net/rpc"
	"utility"
	"utility/service"
)

type RpcServer struct{
	tcplistner *net.TCPListener
	IP   string `json:"ip"`
	PORT string `json:"port"`
}

func (r *RpcServer)InitRpcServer(ip string,port string) {
	r.tcplistner = service.GetListnerPara(ip,port)
	if ip == "" {
		ip = utility.GetIP()
	}
	if port == "0" {
		port = utility.GetPort(r.tcplistner)
	}
	r.IP = ip
	r.PORT = port
}

func (r *RpcServer)StartRpcServer(listener interface{}) {
	rpc.Register(listener)
	rpc.Accept(r.tcplistner)
}