import subprocess
import argparse


#return [count,size]
def getSizes():
    sizes = {}
    sizes["1K"] = [5000,1]
    sizes["10K"] = [5000,10]
    sizes["100K"] = [500,100]
    sizes["1M"] = [50,1024]
    sizes["10M"] = [5,1024*10]
    sizes["100M"] = [5,1024*100]
    sizes["1G"] = [1,1024*1024]
    return sizes

#size in KB
def os_system_dd(test_type,count,size,path):
    prefix = test_type+"test"
    a = []
    for x in xrange(0,count):
        test = prefix + str(x)
        cmd_list = ['dd','if=/dev/zero', 'of='+path+test,'bs=1024' ,'count='+str(size)]
        process = subprocess.Popen(cmd_list)
        process.wait()
   		
#dd if=/dev/zero of=test.dat bs=1024 count=4000000
if __name__ == '__main__':
    count = 0 
    size = 1  
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--size')
    parser.add_argument('-d', '--dir')
    args = parser.parse_args()
    print args.size
    os_system_dd(args.size,getSizes()[args.size][count],getSizes()[args.size][size],args.dir)

