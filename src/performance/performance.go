package main

import (
	"fmt"
	"flag"
	"peer"
	"serverList"
	"strconv"
	"time"
	"math/rand"
	"strings"
	"io/ioutil"
	"os"
)


func main() {


	config,dir,op,pf,testcase := getUserParamaters()
	
	if testcase == "1st" {
		testcase_1(config,dir,op,pf)
	}else {
		testcase_2(config,dir,op,pf,testcase)
	}
	
}


func getUserParamaters() (string,string,int,int,string) {
	config := flag.String("config", "./servers.yml", "Config file: Path of file storing server list")
	dir := flag.String("dir", ".", "Client's directory")
	operations := flag.Int("op", 10000, "operations to perform")
	parallelfactor := flag.Int("pf", 1, "parallel clients")
	testcase := flag.String("tc", "1st", "testcase")
	flag.Parse()
	return *config,*dir,*operations,*parallelfactor, *testcase
}

func testcase_1(config string,dir string,op int, pf int) {
	var peers []peer.Id
	var userinput string
	servers := new(serverList.ServerList)
	peer.RegisterTypes()
	Peer := new(peer.Peer)
	err := servers.InitServerList(config)
	if err!=nil {
		fmt.Println("Yaml: Configuration Error")
		return
	}
	err = Peer.InitPeer(servers,dir)
	if err!=nil {
		fmt.Println(err)
		return
	}
	Peer.Register()
	rand.Seed(int64(time.Now().Nanosecond()))
	var keys [][]string
	var searchKeys []string
	var a string
	id := rand.Intn(pf)
	keys = make([][]string, op)
	tmpDir,_ := ioutil.TempDir(".", "client_dir_")
	out,err := ioutil.TempFile(".", "op_")

	for i:=0; i < op; i++ {
		a = ""
		for j := 0; j < 10; j++ {
			searchKeys = append(searchKeys,strconv.Itoa(id)+strconv.Itoa(i)+strconv.Itoa(j)) 
			a += " "+strconv.Itoa(id)+strconv.Itoa(i)+strconv.Itoa(j)
		}
		keys[i] = strings.Split(a," ")
	}

	fmt.Println("Press Enter for Register")
	fmt.Scanf("%s",&userinput)
	now := time.Now()
	for i := 0; i < op; i++ {
		Peer.Filelist = keys[i]
		Peer.Register()
	}
	duration := time.Since(now)
	fmt.Printf("Register(%d):\nResponse time %s,Avg Response time: %s\n",op,duration,(duration/time.Duration(op)))
	s:=fmt.Sprintf("register: %f\n",(duration/time.Duration(op)).Seconds())
	out.WriteString(s)
	fmt.Println("Press Enter for Search")
	fmt.Scanf("%s",&userinput)
	now = time.Now()
	for i := 0; i < op; i++ {
		peers = Peer.Search(searchKeys[i])
	}	
	duration = time.Since(now)
	fmt.Printf("Search(%d):\nResponse time %s,Avg Response time: %s\n",op,duration,(duration/time.Duration(op)))
	s = fmt.Sprintf("search: %f\n",(duration/time.Duration(op)).Seconds())
	out.WriteString(s)
	
	fmt.Println("Press Enter for Obtain")
	fmt.Scanf("%s",&userinput)
	peers = Peer.Search("test1")
	now = time.Now()
	for i := 1; i < op; i++ {
		Peer.Obtain(tmpDir, "test"+strconv.Itoa((i%50)+1),peers,rand.Intn(pf))
	}
	duration = time.Since(now)
	fmt.Printf("Obtain(%d):\nResponse time %s,Avg Response time: %s\n",op,duration,(duration/time.Duration(op)))
	s = fmt.Sprintf("obtain: %f\n",(duration/time.Duration(op)).Seconds())
	out.WriteString(s)
	os.RemoveAll(tmpDir)
	fmt.Println("Test Done")
	fmt.Scanf("%s",&userinput)
	out.Close()
}

func testcase_2(config string,dir string,op int, pf int, testcase string) {
	
	var peers []peer.Id
	var userinput string
	files,_ := ioutil.ReadDir(dir)
    num_files := len(files)
	var bytes_per_second []float64

	servers := new(serverList.ServerList)
	peer.RegisterTypes()
	Peer := new(peer.Peer)
	err := servers.InitServerList(config)
	if err!=nil {
		fmt.Println("Yaml: Configuration Error")
		return
	}
	err = Peer.InitPeer(servers,dir)
	if err!=nil {
		fmt.Println(err)
		return
	}
	rand.Seed(int64(time.Now().Nanosecond()))
	
	Peer.Register()

	fmt.Println("Press Enter for Obtain")
	fmt.Scanf("%s",&userinput)
	peers = Peer.Search(testcase + "test0")

	tmpDir,_ := ioutil.TempDir(".", "client_dir_")
	out,err := ioutil.TempFile(".", "op_")
	
	for i := 0; i < num_files; i++ {
		_,speed := Peer.Obtain(tmpDir, testcase+"test"+strconv.Itoa(i),peers,rand.Intn(pf))
		bytes_per_second = append(bytes_per_second,speed)
	}	

	tot := 0.0
	for _,value := range bytes_per_second {
		tot += value
	}
	tot /= float64(num_files)

	out.WriteString(testcase + ": " + strconv.FormatFloat(tot, 'f', 6, 64)+"\n")
	os.RemoveAll(tmpDir)
	out.Close()
	
	fmt.Println("Test Done")
	fmt.Scanf("%s",&userinput)
}


